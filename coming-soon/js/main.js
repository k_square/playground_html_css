const countdown = document.querySelector('.countdown');

// Set launch date
const launchDate = new Date('Aug 31, 2018 20:00:00').getTime();

// console.log(launchDate);

//Update every second

const intvl = setInterval(() => {
    // get todays date and time (ms)
    const now = new Date().getTime();

    const distance = launchDate - now;

    // console.log(distance);

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    //Display Result
    countdown.innerHTML = `
        <div>${days}<span>Days</span></div>
        <div>${hours}<span>Hours</span></div>
        <div>${mins}<span>Minutes</span></div>
        <div>${seconds}<span>Seconds</span></div>
    `;

    //Launch date passed
    if (distance < 0) {

        clearInterval(intvl);

        countdown.style.color = '#17a2b8';
        countdown.innerHTML = 'Launched!';
    }
}, 1000);