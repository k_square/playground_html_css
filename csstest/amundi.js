setInterval(() => {
    const date = new Date();

    document.querySelector('.heure').textContent = String(date.getHours()).padStart(2, '0');
    document.querySelector('.minute').textContent = String(date.getMinutes()).padStart(2, '0');
    document.querySelector('.seconde').textContent = String(date.getSeconds()).padStart(2, '0');
}, 200);
